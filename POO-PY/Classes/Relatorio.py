import os, sys

from sqlalchemy import null
currentdir = os.path.dirname(os.path.realpath(__file__)) 
parentdir = os.path.dirname(currentdir) 
sys.path.append(parentdir)

from datetime import date
from Config.config import *
from Classes.Pessoa import Pessoa
from Classes.Colaborador import Colaborador
from Classes.Tanque import Tanque
from Classes.Visitante import Visitante
from Classes.Animal import *
from Classes.Endereco import Endereco
from Classes.Insumo import Insumo
from Classes.InsumoAnimal import InsumoAnimal
from Classes.ProdutoTratamento import ProdutoTratamento
from Classes.ProdutoTratamentoTanque import ProdutoTratamentoTanque
from Classes.Fornecedor import Fornecedor

def gera_relatorio(animal):
    print("Relatório do animal")
    print("Nome: " + animal.nome)  
    print("Idade: " + str(animal.idadeAno))
    print("Espécie: " + animal.especie)
    print("Tanque: " + animal.tanque.nome)
    print("Tamanho do tanque (litros): " + str(animal.tanque.volumeLitros))
    print("Colaborador responsável pelo tanque: " + animal.tanque.colaborador.nome)
    print("Turno do colaborador: " + animal.tanque.colaborador.turno)
    insumos = db.session.query(InsumoAnimal).filter(InsumoAnimal.animal == animal).all()
    for ins in insumos:
        print("Nome do insumo consumido pelo animal: " + ins.insumo.nome_insumo)
        print("Dosagem do insumo: " + str(ins.dosagem))
        print("Periodicidade do insumo: " + ins.periodicidade)

    prod_tratamento = db.session.query(ProdutoTratamentoTanque).filter(ProdutoTratamentoTanque.tanque == animal.tanque).all()
    for prod_trat in prod_tratamento:
        print("Produto utilizado para tratar o tanque: " + prod_trat.produto_tratamento.nome_produto)
        print("Dosagem do produto: " + str(prod_trat.dosagem))
        print("Periodicidade do produto: " + prod_trat.periodicidade)
        print("Fornecedor do produto: " + (prod_trat.produto_tratamento.fornecedor.fornecedor_fisico.nome))