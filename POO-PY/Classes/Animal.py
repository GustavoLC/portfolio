from Config.config import *
from Classes.Tanque import Tanque

class Animal(db.Model):
    __tablename__ = 'Animal'

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    especie = db.Column(db.String(254))
    idadeAno = db.Column(db.Integer)
    tanque_id = db.Column(db.Integer, db.ForeignKey(Tanque.id), nullable=False)
    tanque = db.relationship("Tanque")


    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return f"{self.id}, {self.nome}, {self.especie}, {self.idadeAno}, {str(self.tanque)}"  

