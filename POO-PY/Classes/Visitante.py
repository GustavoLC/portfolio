from Config.config import *
from Classes.Pessoa import Pessoa


class Visitante(Pessoa):
    __tablename__ = 'Visitante'

    cpf = db.Column(db.String(11), db.ForeignKey('Pessoa.cpf', ondelete="CASCADE"), primary_key=True)
    clienteVip = db.Column(db.Boolean)
    ultimasVisitas = db.Column(db.String(254))

    # a identidade polimórfica da classe será armazenada 
    # no campo type da classe pai
    __mapper_args__ = { 
        'polymorphic_identity':'visitante'
    }

    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return f"Colaborador('{self.nome}', '{self.ultimasVisitas}', '{self.clienteVip}')"