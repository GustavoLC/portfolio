from Config.config import *
from Classes.Pessoa import Pessoa
from Classes.Endereco import Endereco


class Fornecedor(db.Model):
    __tablename__ = 'Fornecedor'

    id = db.Column(db.Integer, primary_key=True)
    cnpj = db.Column(db.Integer)
    razao_social = db.Column(db.String(100))
    fornecedor_fisico_cpf = db.Column(db.String(100), db.ForeignKey(Pessoa.cpf))
    fornecedor_fisico = db.relationship("Pessoa")
    endereco_juridico_id = db.Column(db.String(100), db.ForeignKey(Endereco.id))
    endereco_juridico = db.relationship("Endereco")

    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return f"{self.id}, {self.cnpj}, {self.razao_social}, {str(self.fornecedor_fisico)}, {str(self.endereco_juridico)}"