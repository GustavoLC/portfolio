from Config.config import *
from Classes.Colaborador import Colaborador

class Tanque(db.Model):
    __tablename__ = 'Tanque'
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    volumeLitros = db.Column(db.Integer)
    colaborador_cpf = db.Column(db.Integer, db.ForeignKey(Colaborador.cpf))
    colaborador = db.relationship("Colaborador")


    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return f"{self.id}, {self.nome}, {self.volumeLitros}, {str(self.colaborador)}"  