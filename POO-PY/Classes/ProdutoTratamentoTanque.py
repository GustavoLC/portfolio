from Config.config import *
from Classes.ProdutoTratamento import ProdutoTratamento
from Classes.Tanque import Tanque


class ProdutoTratamentoTanque(db.Model):
    __tablename__ = 'ProdutoTratamentoTanque'

    id = db.Column(db.Integer, primary_key=True)
    dosagem = db.Column(db.Float)
    periodicidade = db.Column(db.String(100))
    tanque_id = db.Column(db.String(100), db.ForeignKey(Tanque.id))
    tanque = db.relationship("Tanque")
    produto_tratamento_id = db.Column(db.String(100), db.ForeignKey(ProdutoTratamento.id))
    produto_tratamento = db.relationship("ProdutoTratamento")

    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return f"{self.id}, {self.dosagem}, {self.periodicidade}, {str(self.tanque)}, {str(self.produto_tratamento)}"