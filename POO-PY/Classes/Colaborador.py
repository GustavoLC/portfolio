from Config.config import *
from Classes.Pessoa import Pessoa


"""
Return que depende da classe Pessoa

return f"Colaborador('{self.nome}', '{self.cpf}', '{self.dtnasc}', '{self.salario}', '{self.turno}')"
"""

class Colaborador(Pessoa):
    __tablename__ = 'Colaborador'

    cpf = db.Column(db.String(11), db.ForeignKey('Pessoa.cpf', ondelete="CASCADE"), primary_key=True)
    salario = db.Column(db.Integer)
    turno = db.Column(db.String(254))
    
    __mapper_args__ = { 
        'polymorphic_identity':'colaborador'
    }

    """
    return que está utilizando a classe pessoa do Model.model
    """

    def __str__(self):
        return f"Colaborador('{self.nome}', '{self.salario}', '{self.turno}')"