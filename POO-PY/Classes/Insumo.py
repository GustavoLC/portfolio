from Config.config import * 
from Classes.Fornecedor import Fornecedor

class Insumo(db.Model):
    __tablename__ = 'Insumo'

    id = db.Column(db.Integer, primary_key=True)
    nome_insumo = db.Column(db.String(100), nullable=False)
    fornecedor_id = db.Column(db.Integer, db.ForeignKey(Fornecedor.id))
    fornecedor = db.relationship("Fornecedor")


    def __str__(self):
        return f"{self.id}, {self.nome_insumo}, {self.fornecedor}"