from Config.config import *
from Classes.Insumo import Insumo
from Classes.Animal import Animal


class InsumoAnimal(db.Model):
    __tablename__ = 'InsumoAnimal'

    id = db.Column(db.Integer, primary_key=True)
    dosagem = db.Column(db.Float)
    periodicidade = db.Column(db.String(100))
    animal_id = db.Column(db.Integer, db.ForeignKey(Animal.id))
    animal = db.relationship("Animal")
    insumo_id = db.Column(db.Integer, db.ForeignKey(Insumo.id))
    insumo = db.relationship("Insumo")

    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return f"{self.id}, {self.dosagem}, {self.periodicidade}, {str(self.animal)}, {str(self.insumo)}"