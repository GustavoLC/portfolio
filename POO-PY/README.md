Programação Orientada a Objetos II

Objetivos do Trabalho:
* Linguagem: Python
* Persistência de Dados.
* Criação de Bibliotecas.
* Reusabilidade.
* Orientação a Objetos.


Grupo:
* Gustavo Lofrese Carvalho
* Eduardo Meneghim Alves Silva
