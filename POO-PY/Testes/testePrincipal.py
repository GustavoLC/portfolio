import os, sys

from sqlalchemy import null
currentdir = os.path.dirname(os.path.realpath(__file__)) 
parentdir = os.path.dirname(currentdir) 
sys.path.append(parentdir)

from datetime import date
from Config.config import *
from Classes.Pessoa import Pessoa
from Classes.Colaborador import Colaborador
from Classes.Tanque import Tanque
from Classes.Visitante import Visitante
from Classes.Animal import Animal
from Classes.Endereco import Endereco
from Classes.Insumo import Insumo
from Classes.InsumoAnimal import InsumoAnimal
from Classes.ProdutoTratamento import ProdutoTratamento
from Classes.ProdutoTratamentoTanque import ProdutoTratamentoTanque
from Classes.Fornecedor import Fornecedor
from Classes.Relatorio import *

from Config.criaTabelas import apaga_e_cria_banco

apaga_e_cria_banco()

#-------------------Endereço-------------------

e1 = Endereco(
    logradouro = "Rua Rogério Giorgi",
    numero = "781",
    cep = "03431000",
    bairro = "Vila Carrão",
    estado = "São Paulo",
    pais = "Brasil"
)
e2 = Endereco(
    logradouro = "Rua XV de Novembro",
    numero = "1444",
    cep = "40028922",
    bairro = "Centro",
    estado = "Blumenau",
    pais = "Brasil"
)
e3 = Endereco(
    logradouro = "Rua Gomes de Carvalho",
    numero = "104",
    cep = "89456587",
    bairro = "Santo Amaro",
    estado = "São Paulo",
    pais = "Brasil"
)
db.session.add(e1)
db.session.add(e2)
db.session.add(e3)
db.session.commit()
print(e1)
print(e2)
print(e3)

#-------------------Pessoa-------------------

p1 = Pessoa(
    cpf = "12335678910",
    nome = "João da Silva", 
    dataNascimento = date(2014,1,1),
    endereco = e1)
p2 = Pessoa(
    cpf = "13345478911",
    nome = "Maria Joaquina", 
    dataNascimento = date(2014,1,1))
p3 = Pessoa(
    cpf = "203457278911",
    nome = "Maria Fernanda", 
    dataNascimento = date(2014,1,1))
db.session.add(p1)
db.session.add(p2)
db.session.add(p3)
db.session.commit()
print(p1)
print(p2)
print(p3)

#-------------------Colaborador-------------------

f1 = Colaborador(
    cpf = "50256104867",
    nome = "Gustavo",
    dataNascimento = date(2001,1,1),
    endereco = e1,
    salario = 1235, 
    turno = 1)
f2 = Colaborador(
    cpf = "50256104477",
    nome = "Eduardo",
    dataNascimento = date(2002,11,1),
    salario = 1300, 
    turno = 3)
db.session.add(f1)
db.session.add(f2)
db.session.commit()
print(f1)
print(f2)

#-------------------Tanque-------------------

t1 = Tanque(
    nome="Atlântico 2", 
    volumeLitros=50,
    colaborador = f1)
t2 = Tanque(
    nome="Pacifico 3", 
    volumeLitros=120,
    colaborador = f2)

db.session.add(t1)
db.session.add(t2)
db.session.commit()
print(t1)
print(t2)

#-------------------Visitante-------------------

v1 = Visitante(
    cpf = "40856104477",
    nome = "Kami",
    dataNascimento = date(2005,12,19),
    clienteVip = 1,
    ultimasVisitas = "15-03-22, 30-4-22"
)
v2 = Visitante(
    cpf = "54845115457",
    nome = "Nicky",
    dataNascimento = date(2002,2,19),
    clienteVip = 0,
    ultimasVisitas = "15-03-22"
)
v3 = Visitante(
    cpf = "85236974145",
    nome = "Gabriel",
    dataNascimento = date(2008,10,30),
    clienteVip = 1,
    ultimasVisitas = "14-05-20, 15-05-20, 06-07-21, 09-11,22"
)
db.session.add(v1)
db.session.add(v2)
db.session.add(v3)
db.session.commit()
print(v1)
print(v2)
print(v3)

#-------------------Animal-------------------

a1 = Animal(
    nome="Bodião-canário",
    especie="Labrus mixtus",
    idadeAno = 7,
    tanque = t1
)

a2 = Animal(
    nome="Cavalo Marinho",
    especie="Hippocampus guttulatus",
    idadeAno = 3,
    tanque = t1
)

a3 = Animal(
    nome="Blênio-das-cracas-de-Cortez",
    especie="Acanthemblemaria hastingsi",
    idadeAno = 1,
    tanque = t2
)

a4 = Animal(
    nome="Bodião-de-barbatana-multicolorida",
    especie="Parajulis poecilopterus",
    idadeAno = 10,
    tanque = t2
)
db.session.add(a1)
db.session.add(a2)
db.session.add(a3)
db.session.add(a4)
db.session.commit()
print(a1)
print(a2)
print(a3)
print(a4)

#-------------------Fornecedor-------------------

forn1 = Fornecedor(
    cnpj = 10635424000186,
    razao_social = "Fornecedor empresa",
    endereco_juridico = e1
)
forn2 = Fornecedor(
    fornecedor_fisico = p1
)
db.session.add(forn1)
db.session.add(forn2)
db.session.commit()
print(forn1)
print(forn2)

#-------------------Insumo-------------------

ins1 = Insumo(
    nome_insumo = "Dorflex",
    fornecedor = forn1
)
db.session.add(ins1)
db.session.commit()
print(ins1)

#-------------------InsumoAnimal-------------------

insAnimal1 = InsumoAnimal(
    animal = a1,
    insumo = ins1,
    dosagem = 5.5,
    periodicidade = "Diário"
)
db.session.add(insAnimal1)
db.session.commit()
print(insAnimal1)

#-------------------ProdutoTratamento-------------------


prodTratamento1 = ProdutoTratamento(
    nome_produto = "Filtro MegaHiper5200",
    fornecedor = forn2
)
db.session.add(prodTratamento1)
db.session.commit()
print(prodTratamento1)

#-------------------ProdutoTratamentoTanque-------------------

prodTratamentoTanque1 = ProdutoTratamentoTanque(
    tanque = t1,
    produto_tratamento = prodTratamento1,
    dosagem = 1,
    periodicidade = "Mensal"
)
db.session.add(prodTratamentoTanque1)
db.session.commit()
print(prodTratamentoTanque1)

gera_relatorio(a1)