#ifndef J1_QUADRILATERAL_H
#define J1_QUADRILATERAL_H

/**
*@author Gustavo Lofrese Carvalho, Eder
*@date 08/11/2021
*@version 11/01/2022
*@brief An abstract Class to store a Quadrilateral
**/

#include "polygon.h"
#include "window.h"

class Quadrilateral:public Polygon 
{
  private:
    float base;
    float altura;
  public:
		Quadrilateral(Point2 p[]);
    Quadrilateral();
    ~Quadrilateral();

};

#endif
