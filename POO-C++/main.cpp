#include <iostream>
#include <vector>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <string>

using namespace std;

#include "window.h"
#include "sdl.h"
#include "shape.h"
#include "polygon.h"
#include "quadrilateral.h"
#include "circle.h"
#include "duet.h"
#include "retangulo.h"
#include "jogo.h"
#include "matriz.h"

int main(int argc, char *argv[])
{
    //Utiliza o getInstance para instanciar o sdl
    SDL &sdl = SDL::getInstance();
    sdl.init();    
    bool quit = false;


    

    Window w(800, 800);
    Jogo jogo(w.width + w.height);

    Duet duet
    (   
        Point2(w.width/2,w.height*75/100),    //ponto central
        120,                 //raio
        Circle(400,600,30), //c1
        Circle(400,600,30), //c2
        0                   //alpha
    );
    duet.rotacao(0);


    /**
     * @brief Vector que irá acumular os retangulos
     * @authors Gustavo Lofrese Carvalho, Eduardo Meneghim, Eder Penharbel
     * @date 01/12/2021
     */
    vector<Retangulo*> retangulos;
    int t = 0;

   

	//Event handler
	SDL_Event e;
    {
        srand(time(NULL));

        // Loop de Jogo
        while (!quit) 
        {
            /**
            *@author Gustavo Lofrese Carvalho, Eduardo Meneghim, Eder Augusto Penharbel
            *@date 29/11/2021
            *@version 13/12/2021
            * @brief gera retangulos automaticamente
            * @param t tempo de execucao
            **/  
    
            if (t % jogo.intervalo == 0)
            {

                int altura = jogo.sortearNumero(100,150); //Sorteia a altura do retangulo
                int base = altura * 0.7; //Usa a altura para definir o tamanho da base

                // Define se o retângulo será horizontal ou vertical (50% de chance cada)
                if (altura <= 115)
                    base = base / 3;
                else
                    altura = altura / 3;

                Retangulo* r1 = new Retangulo
                                    (Point2(jogo.sortearNumero(w.width*30/100,w.width*70/100), //posicao x
                                            -(jogo.sortearNumero(130,160))), //posicao y 
                                    base,
                                    altura,
                                    false);
                
                
                // Faz um retângulo girar, outro não
                if (retangulos.size() > 0)
                    r1->deveGirar = !retangulos.back()->deveGirar;

                retangulos.push_back(r1);
            }
            t++;

            // Call the Score function         
            jogo.dificuldade();


            // Handle events on queue
            while(SDL_PollEvent(&e))
            { 
                //comandos de teclado, fazer os dois circulos se movimentarem
                if(e.type == SDL_KEYDOWN)
                {
                    switch( e.key.keysym.sym )
                    {
                        case SDLK_LEFT:
                            duet.rotacao(-(M_PI/30));                           
                            break;

                        case SDLK_RIGHT:
                            duet.rotacao(M_PI/30);
                            break;
                        
                        default:
                        break;

                    }
                }

                // User requests quit
                if(e.type == SDL_QUIT)
                {
                    jogo.imprimePontuacao();
                    quit = true;
                }
            }
            
            
            // clear surface
            w.clear();
            
            /**
            *@author Gustavo Lofrese Carvalho, Eduardo Meneghim
            *@date 01/12/2021
            *@version 31/01/2022
            *@brief Gera, desenha, movimenta, faz a colisão e remove os retangulos
            **/
            for (size_t i = 0; i < retangulos.size(); i++)
            {
                
                retangulos[i]->draw(w); //desenha os retangulos
                retangulos[i]->transladar(0,jogo.velocidade); // desce retangulos automaticamente de acordo com a velocidade
                
                if ((duet.c1.colisaoCQ(retangulos[i]) || duet.c2.colisaoCQ(retangulos[i])) &&     
                    (retangulos[i]->colisaoR(duet.c1.c, duet.c1.r) || retangulos[i]->colisaoR(duet.c2.c, duet.c2.r)))
                {
                    jogo.imprimePontuacao();
                    sdl.quit();
                    return 0;  
                }
                
               
                // Apaga os retângulos assim que eles saem da tela
                if (retangulos[i]->getMinY() > w.height + 200)
                    retangulos.erase(retangulos.begin());

                //Se o retângulo possuir deveGirar = true, ele gira
                if (retangulos[i]->deveGirar == true)
                    retangulos[i]->rotacao(M_PI/3000);  //angulo
                
                
            }

            // draw duet
            duet.draw(w);

            // update renderer
            w.update();
        }
        // Fim do loop de jogo
    }
    sdl.quit();
    return 0;
}


/*
- Transladar,Rotação do retângulo ✔
    * Criar o polimorfismo do rotação, alterar "aumentarAngulo" do duet para rotação 
    * Remover addX e addY, utilizar o Transladar ✔
    * Criar o Transladar inicial, matriz identidade
- Pontuação na tela
- Interface - "Game Over", "Start"
- Ajustar formula dos retangulos (tamanho de acordo com a tela)
- Adicionar sistema de deixar o jogo mais dificil com o passar do tempo ✔
*/