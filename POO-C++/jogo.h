#ifndef JOGO_H
#define JOGO_H

#include <stdlib.h>   
#include <string>
#include <fstream>
#include <iostream>
#include <ctime>
#include <math.h>  

using namespace std;

/**
 * @authors Gustavo Lofrese Carvalho
 * @date 11/12/2021
 * @version 06/02/2022
 * @brief An Class to store Game stuffs
**/
class Jogo
{
    private:
        int pontuacao;

    public:
        Jogo(int tamanhoTela);
        Jogo();
        ~Jogo();

        int getPontuacao();
        void dificuldade();
        string getLastLine(fstream& in);
        void imprimePontuacao();
        int sortearNumero(int min, int max);

        double velocidade;
        int intervalo;

};




#endif