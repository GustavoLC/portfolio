/**
 * Implementação da classe Matriz
 * 
 * \author Gustavo Lofrese, Eder
 * \date 25/10/2021
 * \version 17/12/2021
 * 
**/

#include "matriz.h"
#include <iostream>
using namespace std;

/**
 * @brief Construct a empty new Matriz:: Matriz object
 * Construtor padrão
 * @param linhas linhas da matriz
 * @param colunas colunas da matriz
 */
Matriz::Matriz(int linhas, int colunas):
    linhas{linhas},         //iniciando membros
    colunas{colunas},
    a{nullptr}
{
    a = new double *[linhas];


    for (int i = 0; i < linhas; i++)
    {
        a[i] = new double [colunas];

        for (int c = 0; c < colunas; c++) 
            a[i][c] = 0;
    }
    
}

/**
 * @brief Construct a new Matriz:: Matriz object
 * Construtor de cópia
 * @param m the matrix
 */
Matriz::Matriz(const Matriz& m):
    linhas{m.linhas},       //iniciando membros
    colunas{m.colunas},
    a{nullptr}

{
    a = new double *[linhas];


    for (int i = 0; i < linhas; i++)
    {
        a[i] = new double [colunas];

        for (int c = 0; c < colunas; c++) 
            a[i][c] = m.a[i][c];
    
    }
    
}

/**
 * @author Gustavo Lofrese Carvalho
 * @brief Construct a new Matriz:: Matriz object
 * Construtor de movimentação
 * @date 04/02/2022
 * @version 04/02/2022
 * @param m 
 */
Matriz::Matriz(Matriz &&m):
    linhas{m.linhas},       //iniciando membros
    colunas{m.colunas},
    a{m.a}
{
    m.linhas = 0;
    m.colunas = 0;
    m.a = nullptr;
}

//destrutor
Matriz::~Matriz()
{
    if (a != nullptr)
    {
        for (int i = 0; i < linhas; i++)
            delete [] a[i];


        delete [] a;
    }
}

//imprimir a Matriz 
void Matriz::imprimir()
{
    for (int i = 0; i < linhas; i++)
    {
        for (int c = 0; c < colunas; c++) 
            cout << a[i][c] << " " ;
    cout << "\n";        
    }
    cout << "\n"; 
}

//inicializar uma Matriz com valores inseridos
void Matriz::inserirValores(int linhas, int colunas, double valor)
{
    a[linhas-1][colunas-1] = valor;   
}

//operador para somar uma Matriz com número inteiro(pode utilizar (a + (-2)), por exemplo)
Matriz& Matriz::operator +=(int v)
{
    for (int i = 0; i < linhas; i++)
    {
        for (int c = 0; c < colunas; c++) 
            a[i][c] += v;    
    }    
    return *this;
}

//operador para somar Matriz com Matriz
Matriz& Matriz::operator +=(const Matriz& m)
{
    for (int i = 0; i < linhas; i++)
    {
        for (int c = 0; c < colunas; c++) 
            a[i][c] += m.a[i][c];    
    }    
    return *this;
}

//operador para multiplicar Matriz com número inteiro
Matriz& Matriz::operator *=(int v)
{
    for (int i = 0; i < linhas; i++)
    {
        for (int c = 0; c < colunas; c++) 
            a[i][c] *= v;    
    }    
    return *this;
}


/**
 * Atualização: Multiplicação de matrízes quadradas
 * 
 * \author Gustavo Lofrese
 * \date 30/10/2021
 * 
**/

//operador para multiplicar Matriz com Matriz (somente quadrada)
Matriz& Matriz::operator *=(const Matriz& m)
{
    for(int i = 0; i < linhas; i++) 
    {
		for(int c = 0; c < colunas; c++) 
			a[i][c] *= m.a[i][c];
	}
    return *this;
}

//operador igual, deleta a Matriz original e retorna a nova
Matriz& Matriz::operator =(const Matriz& m)
{
    if (a != nullptr)
    {
        for (int i = 0; i < linhas; i++)
            delete [] a[i];


        delete [] a;
    } 
    
    linhas = m.linhas;

    a = new double *[linhas];


    for (int i = 0; i < linhas; i++)
    {
        a[i] = new double [colunas];

        for (int c = 0; c < colunas; c++) 
            a[i][c] = m.a[i][c];
    }

    return *this;
}


//função amiga, soma de Matriz com inteiro
Matriz operator +(const Matriz& m, int v){
    Matriz tmp = m;
    tmp += v;

    return tmp;
}

//função amiga, soma de inteiro com Matriz
Matriz operator +(int v, const Matriz& m){
    return m + v;
}

//função amiga, soma de Matriz com Matriz
Matriz operator +(const Matriz& m, const Matriz& n){

    if (m.linhas != n.linhas || m.colunas != n.colunas)
    {
        cout << "Impossível somar ou subtrair as Matrizes inseridas \n \n";
        exit(1);
    } 
    
    Matriz tmp = m;
    return tmp += n;
}

//função amiga, multiplicação de Matriz por inteiro
Matriz operator *(const Matriz& m, int v){
    Matriz tmp = m;
    tmp *= v;

    return tmp;
}

//função amiga, multiplicação de inteiro por Matriz
Matriz operator *(int v,const Matriz& m){
    return m * v;
}


/**
 * @authors Gustavo Lofrese, Eduardo Meneghim
 * @brief Multiplicação de Matrizes
 * @date 17/12/2021
 * @version 17/12/2021
 * @param m Matriz 1
 * @param n Matriz 2
 * @return Matriz resultado
 */
Matriz operator *(const Matriz& m, const Matriz& n){

    if (m.colunas != n.linhas)
    {
        cout << "Impossível multiplicar as Matrizes inseridas \n \n";
        exit(1);
    } 
    

    Matriz tmp(m.linhas, n.colunas);

    for (int i = 0; i < m.linhas; i++)
    {
        for (int j = 0; j < n.colunas; j++)
        {
            tmp.a[i][j] = 0;
            for (int k = 0; k < m.colunas; k++)
            {
                tmp.a[i][j] += m.a[i][k] * n.a[k][j];
            }
        }
    }

    return tmp;
}
