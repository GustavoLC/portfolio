#ifndef J1_POINT2_H
#define J1_POINT2_H

/**
* \author Éder Augusto Penharbel
* \date 22/11/2021
* \brief An abstract Class who gets one point with two coordinates
**/
class Point2 {
    public:
    	Point2();
		Point2(double x, double y);
        Point2(const Point2 &p);

		double x; /** x coordinate of the point */
		double y; /** y coordinate of the point */

};

#endif
