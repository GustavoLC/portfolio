#include "quadrilateral.h"

/**
* @author Gustavo Lofrese Carvalho, Eder
* @date 08/11/2021
* @version 08/11/2021
**/

Quadrilateral::Quadrilateral():
	Polygon(4)
{}

Quadrilateral::Quadrilateral(Point2 p[]):
	Polygon(4,p)
{}

Quadrilateral::~Quadrilateral()
{}

/*Quadrilateral::Quadrilateral(float base, float altura):
	base{base},
	altura{altura},
	Polygon(4,Point2 p[] = { Point2(0, 0), Point2(0, 0), Point2(0, 0), Point2(0,0)})
{
	int padrao = 100; 
	base += padrao;
	altura += padrao;
	p[] = { Point2(padrao, padrao), Point2(base, padrao), Point2(base, altura), Point2(padrao,altura)};
}*/

