#ifndef J1_SDL_H
#define J1_SDL_H

#include "Singleton.h"

/**
 * @brief SDL utilizando Singleton - Evitar cópias
 * @author Gustavo Lofrese Carvalho
 * @date 19-01-2022
 * @version 09/02/2022
 */

class SDL : public Singleton <SDL>
{
    private:
    //Privar o construtor para não ser possível a criação de instâncias
    SDL() = default;

    public:
        // SDL consegue acessar os métodos da classe Singleton
        friend class Singleton<SDL>;
        
        bool init();
        bool quit();
};

#endif
