Programação Orientada a Objetos I

Objetivos do Trabalho:

* Linguagem: C++.
* Interface gráfica com SDL.
* Lógica matemática.
* Gerenciamento de memória.
* Orientação a Objetos.


Grupo:

* Gustavo Lofrese Carvalho
* Eduardo Meneghim Alves Silva
