#ifndef DUET_H
#define DUET_H

#include "circle.h"


/**
 * @authors Gustavo Lofrese Carvalho, Eduardo Meneghin
 * @date 06/12/2021
 * @version 06/12/2021
 * @brief An abstract Class to store a Duet
**/
class Duet:public Shape
{
public:
    Duet(Point2 p, double raio, Circle c1, Circle c2, double alpha);
    ~Duet();

    Point2 p;
    double raio;
    Circle c1;
    Circle c2;
    double alpha;
    Circle c;

    void rotacao(double da);
    void draw(Window &w);

};




#endif