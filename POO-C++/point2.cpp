#include "point2.h"

Point2::Point2():
	x{0.0}, y{0.0}
{ }

Point2::Point2(double x, double y):
	x{x}, y{y}
{ }
        
/**
* \author Éder Augusto Penharbel
* \date 10/11/2021
* \brief The copy constructor of the class Point2
* \param p a point
**/
Point2::Point2(const Point2 &p):
    Point2{p.x, p.y} // chaining constructor, delegating constructor */
{
}
