#include <stdlib.h>
#include <iostream>

#include "circle.h"
#include <math.h>

using namespace std;



/**
* Constructor
* @authors Gustavo Lofrese Carvalho, Éder Augusto Penharbel
* @date 10/11/2021
* @version 10/11/2021
* @param x, the x coordinate of the center
* @param y, the y coordinate of the center
* @param r, the radius of the circle
* @brief Circle constructor
*/
Circle::Circle(double x, double y, double r):
    c{x,y},
    r{r}
{
}

/**
* Constructor
* @authors Gustavo Lofrese Carvalho, Éder Augusto Penharbel
* @date 10/11/2021
* @version 10/11/2021
* @param c, the center of the circle
* @param r, the radius of the circle
* @brief Circle constructor with Point2
*/
Circle::Circle(const Point2 &c, double r):
    c{c},
    r{r}
{
}


/**
 * @authors Gustavo Lofrese Carvalho, Éder Augusto Penharbel
 * @brief Destroy the Circle:: Circle object
 *@date 10/11/2021
 *@version 10/11/2021
 */
Circle::~Circle()
{
}


/**
 * @brief draw the circle in the window
 * @authors Gustavo Lofrese Carvalho
 * @param w, the window
 * @date 10/11/2021
 * @version 10/11/2021
 */
void Circle::draw(Window &w)
{
  w.drawCircle((int)c.x, (int)c.y, (int)r);
}


/**
 * @author Gustavo Lofrese Carvalho, Eduardo Meneghim
 * @date 29/11/2021
 * @version 31/01/2022
 * @brief test for collision between rectangles and circles
 * @param p References a Rectangle
**/
bool Circle::colisaoCQ(Retangulo *p) 
{

  // variáveis temporárias para usarmos nos cálculos
  float valorX = c.x;
  float valorY = c.y;

  // Verifica qual canto é o mais próximo
  if (c.x < p->getMinX())
    valorX = p->getMinX();     // testa esquerda

  else if (c.x > p->getMaxX()) 
    valorX = p->getMaxX();   // testa direita

  if (c.y < p->getMinY())         
    valorY = p->getMinY();      // // testa cima

  else if (c.y > p->getMaxY()) 
    valorY = p->getMaxY();   // testa baixo

  // Pega a distância do canto mais próximo
  float distX = c.x-valorX;
  float distY = c.y-valorY;
  float distancia = sqrt( (distX*distX) + (distY*distY) );

  // Caso a distância seja menor que o raio, significa que há colisão
  if (distancia <= r) {
    return true;
  }
  return false;
}

/**
 * @brief just overwriting the virtual function
 * @date 02/02/2022
 * @version 02/02/2022
 * @author Gustavo Lofrese Carvalho
 * @param da, how much will the circle rotate
 */
void Circle::rotacao(double da) 
{
}
