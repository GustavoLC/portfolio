#include "duet.h"
#include <iostream>

using namespace std;

/**
 * @authors Gustavo Lofrese Carvalho, Eduardo Meneghim
 * @date 06/12/2021
 * @version 06/12/2021
 * @brief Duet Constructor, three Circles: a big one and two small connected to him
 * @param p A Point2 type, center of the big circle
 * @param raio Radius of the big circle
 * @param c1 A Circle type, the first small circle
 * @param c2 A Circle type, the second small circle
 * @param alpha The big circle angle
**/

Duet::Duet(Point2 p, double raio, Circle c1, Circle c2, double alpha):  
    p{p},
    raio{raio},
    c1{c1},
    c2{c2},
    alpha{alpha},
    c{p,raio}
{
}

/**
 * @authors Gustavo Lofrese Carvalho, Eduardo Meneghim
 * @brief Destroy the Duet:: Duet object
 * @date 06/12/2021
 * @version 06/12/2021
 */
Duet::~Duet()
{
}

/** 
 * @authors Gustavo Lofrese Carvalho, Eduardo Meneghim
 * @brief Rotate the circle clockwise and counterclockwise
 * @param alpha how much will the circles move
 */
void Duet::rotacao(double da)
{
    //C1
    alpha += da;

    c1.c.x += (p.x + raio*cos(alpha)) - (c1.c.x); //X Novo - X Antigo;
    c1.c.y += (p.y + raio*sin(alpha)) - (c1.c.y); //Y Novo - Y Antigo
    //--------------------------

    //C2
    double alpha_temp = alpha + M_PI;

    c2.c.x += (p.x + raio*cos(alpha_temp)) - (c2.c.x); //X Novo - X Antigo
    c2.c.y += (p.y + raio*sin(alpha_temp)) - (c2.c.y); //Y Novo - Y Antigo
}

/**
 * @author Gustavo Lofrese Carvalho
 * @brief Draw duet in the window
 * @date 02/02/2022
 * @version 02/02/2022
 * @param w the window
 */
void Duet::draw(Window &w) 
{
    c1.draw(w);
    c2.draw(w);
    c.draw(w);
}

