#ifndef __SINGLETON_H__
#define __SINGLETON_H__

/**
 * @brief Template de Singleton
 * @author Gustavo Lofrese Carvalho
 * @date 09/02/2022
 * @version 09/02/2022
 * @tparam T nome da classe que utilizará o Singleton
 */

template <typename T>
class Singleton
{
    protected:
        //Construtor e destrutor são default
        Singleton() = default;

    public: 
        // Método que cria a instância
        // Se não existe instância, ele cria. Se existe, ele retorna.
        static T& getInstance(){
                static T instance;
                return instance;
            }

        //Apaga o construtor de cópia e o operador de igual
        Singleton(Singleton const&) = delete;
        Singleton operator=(Singleton const&) = delete;
};


#endif // __SINGLETON_H__