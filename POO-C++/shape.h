#ifndef J1_SHAPE_H
#define J1_SHAPE_H

#include "window.h"


/**
* @author Éder Augusto Penharbel, Gustavo Lofrese Carvalho
* @date 11/11/2021
* @version 11/01/2022
* @brief An abstract Class to store a Geometrical Shape
**/
class Shape
{
	protected:
		virtual void draw(Window &w) = 0;
		virtual void rotacao(double da) = 0;
};

#endif
