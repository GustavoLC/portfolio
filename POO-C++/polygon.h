/**
*@author Gustavo Lofrese Carvalho, Eder
*@date 22/11/2021
*@version 11/01/2022
*@brief An abstract Class to store a Polygon
**/

#ifndef POLYGON_H
#define POLYGON_H

#include "shape.h"
#include "point2.h"
#include "window.h"
#include "matriz.h"

class Polygon:public Shape
{
  protected:
    Point2 *p; /** coordinates of the points  */
    int npontos; /** number of geometric shape points */

    double minimox;
		double minimoy;
		double maximox;
		double maximoy;

  public:
    Polygon(int npontos, Point2 p[]);
    Polygon(int npontos);
    ~Polygon();

    void draw(Window &w);
    void transladar(double dx, double dy);
    bool colisao(Polygon& f1, Polygon& f2);
    void rotacao(double da);
    bool colisaoR(const Point2 p, const double r);

    double getMinX() const;
		double getMinY() const;
	  double getMaxX() const;
	  double getMaxY() const;
};

#endif