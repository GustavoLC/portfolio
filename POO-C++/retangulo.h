#ifndef RETANGULO_H
#define RETANGULO_H

#include "quadrilateral.h"
#include "matriz.h"

/**
* @author Gustavo Lofrese Carvalho
* @date 1/12/2021
* @version 20/12/2021
* @brief An abstract Class to store a Rectangle
**/

class Retangulo:public Quadrilateral
{
    public:
        Retangulo(Point2 cimaEsquerda, double base, double altura, bool deveGirar);
        ~Retangulo();
        bool deveGirar = false;
       
};

#endif

